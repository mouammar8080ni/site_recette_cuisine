<?php
require __DIR__."/../config.php" ;
require $GLOBALS['PHP_DIR']."class/Autoloader.php" ;
    Autoloader::register();
    use recipe\RecipeDB;

    $tags = new RecipeDB();
    $results = $tags->getTags();

    /**
     *  Cette page permet de lister les tags disponible
     */
    ob_start();?>
        <div id="ingredients-content">
            <?php
            foreach ($results as $tag): ?>
                <?= $tag->getHTML();
            endforeach;?>
        </div>

    <?php $content = ob_get_clean();
    Template::render($content); ?>